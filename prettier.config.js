module.exports = {
    trailingComma: "es5",
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    endOfLine: "cr",
    printWidth: 120,
    arrowParens: "always",
    jsxSingleQuote: true,
};
